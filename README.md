# 🚴 HugoVelo - Site statique derrière le reverse proxy Traefik

![](screenshot.png)

<!-- TOC -->
* [🚴 HugoVelo - Site statique derrière le reverse proxy Traefik](#-hugovelo---site-statique-derrière-le-reverse-proxy-traefik)
  * [🚀 Kickoff](#-kickoff)
  * [🛠️ Lancer le site web](#-lancer-le-site-web)
  * [🌍Accès au site](#accès-au-site)
<!-- TOC -->

Ce repo contient le code source d'un site web statique généré à l'aide du framework [Hugo](https://gohugo.io/), ainsi que la 
configuration nécessaire à son déploiement derrière le reverse proxy Traefik via `docker-compose`.

## 🚀 Kickoff

`mkcert` permet la génération d'un certificat auto-signé permettant d'avoir du SSL signé en local. Afin d'éviter un 
avertissement de ce type...

![img/screenshot01.png](img%2Fscreenshot01.png)

...il faut installer une autorité de certification locale et générer les certificats correspondants :

```bash
brew install mkcert nss
mkcert -install
```
> Le paquet `nss` permet d'installer l'autorité de certification dans le magasin de sécurité de Firefox

Puis, générer les certificats :

```bash
mkdir certs
mkcert -cert-file certs/local-cert.pem -key-file certs/local-key.pem "docker.localhost" "*.docker.localhost" "domain.local" "*.domain.local"`
```

## 🛠️ Lancer le site web

- En premier lieu, cloner le repo :

```bash
git clone https://gitlab.com/sdv9972401/ci_cd/hugovelo-exercice-3.git
```

- Créer un réseau docker nommé `proxy` :

```bash
docker network create proxy
```

- Executer le fichier `docker-compose.yml` :
```bash
docker compose up -d
```

Le `docker-compose.yml` se charge de builder l'image et de lancer les containers nécessaires.

## 🌍Accès au site

Ouvrir un navigateur et accéder à l'URL https://hugovelo.docker.localhost/

> Le dashboard de `traefik` est disponible à l'adresse http://localhost:8080



